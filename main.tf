provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}

data "aws_ami" "debian" {
  most_recent = true
  owners = ["136693071363"] # Amazon

  filter {
	name = "name"
	values = ["debian-11-amd64-*"]
  }
}

resource "aws_key_pair" "key" {
  key_name = "${var.key_name}"
  public_key = "${file("${var.key_name}.pub")}"
}

resource "aws_instance" "castopod" {
  ami = "${data.aws_ami.debian.id}"
  instance_type = "${var.instance_type}"
  key_name = "${aws_key_pair.key.key_name}"

  tags = {
    Name = "${var.server_name}"
  }

  security_groups = [
    "${aws_security_group.castopod_sg.name}"
  ]

  provisioner "local-exec" {
    command = "sleep 30 && echo \"[castopod]\n${aws_instance.castopod.public_ip} ansible_connection=ssh ansible_ssh_user=admin ansible_ssh_private_key_file=${var.key_name}\" > inventory && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory playbook.yml --extra-vars \"server_hostname=${var.server_name} castopod_server_domain=${var.castopod_zone}.${var.zone} castopod_local_address=${aws_instance.castopod.private_ip} castopod_public_address=${aws_instance.castopod.public_ip} castopod_pkg=${var.castopod_pkg}\""
  }
}

resource "aws_security_group" "castopod_sg" {
  name = "castopod-sg"
  description = "Allow access to Castopod"

  # SSH
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${var.permitted_ssh_cidr_block}"]
  }

  # HTTP
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
