output "private_ip" {
  description = "The private IP address of the castopod server."
  value = "${aws_instance.castopod.private_ip}"
}

output "public_ip" {
  description = "The public IP address of the castopod server."
  value = "${aws_instance.castopod.public_ip}"
}

output "public_dns" {
  description = "The AWS domain name entry of the castopod server."
  value = "${aws_instance.castopod.public_dns}"
}
