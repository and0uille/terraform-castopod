region = "eu-west-3"
availability_zone = "eu-west-3a"
instance_type = "t2.micro"
key_name = "castopod-kp"
server_name = "castopod" # used to set the hostname on the linux box
zone = "chaouane.xyz"
castopod_zone="testcastopod"
permitted_ssh_cidr_block = "0.0.0.0/0" # It is recommend to changed to your IP to prevent noise from SSH scanners
castopod_pkg = "https://code.castopod.org/adaures/castopod/uploads/ab09b21e1e0f3ef02518fc0794d6a3be/castopod-1.1.2.zip"
