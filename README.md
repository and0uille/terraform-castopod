# Castopod server using Terraform and Ansible

This setup was originally taken from [@4ARMED](https://twitter.com/4armed) work
(see
[terraform-burp-collaborator](https://github.com/4ARMED/terraform-burp-collaborator)).

## Introduction

This is a [Terraform](https://terraform.io/) configuration to build a [Castopod
server](https://castopod.org/fr/) on an [Amazon Web Services EC2
Instance](https://aws.amazon.com/). It uses Terraform to create the instance
and then uses a local Ansible role to provision the server.

## *** WARNING ***

Just in case you've been living in a cave, everything in this README will cost
you money on AWS. Even the free tier won't save you as it costs $0.50 per month
for a hosted zone.

4ARMED and/or and0uille are not in any way liable for your infrastructure
costs. You should know by now not to just run things without understanding what
you're doing. :-)

## Steps

### Set up AWS API user

To use this you need to perform a couple of additional steps to be ready to run
Terraform. The first is you need an AWS account and a valid access ID and
secret (create a programmatic-only IAM user). I'm not going to talk through how
to do this as it'll double the length of this document. Sorry! Try here
http://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html.

Once you have these values they can be plugged in to this to configure the [AWS
Provider](https://www.terraform.io/docs/providers/aws/).

### Configure AWS Credentials

This config assumes you will use the AWS CLI credentials store. First install
it if you have not already:

`pip install aws-cli`

Then configure it:

```
aws configure
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-west-2
Default output format [None]: ENTER
```

### Set up Ansible

If you don't have Ansible installed, you will need to do so. I recommend the
devel version, it's very stable. If you choose to go main make sure you have at
least version 2.2.

`pip install git+git://github.com/ansible/ansible.git@devel`

### Clone this repo to a local folder

```
git clone https://repo
cd terraform-castopod
```

It is assumed that everything else in this doc will be performed with the
current working directory in this folder.

### Generate an SSH key pair

We're going to assume you don't have a keypair already in AWS so we'll generate
one now and upload it to AWS. You can skip this step if you already have one,
just update [terraform.tfvars](terraform.tfvars) to use the right
_key_pair_name_ and place the public key file in this directory.

Feel free to use a different comment or algorithm and it's best to set a
passphrase on the key (obvs).

`ssh-keygen -b 2048 -t rsa -C private_castopod@aws -f castopod-kp`

Which will produce output like:
```
Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in castopod-kp.
Your public key has been saved in castopod-kp.pub.
```

Make sure your keypair file has the same name as your _key_pair_name_ variable
as we will look there to upload it to AWS.

### Configure Terraform variables

Edit the file [terraform.tfvars](terraform.tfvars).

```
# Use whatever region you prefer
region = "eu-west-2"

# Here we are using a different AWS profile from default, you don't have to but 
# this is how if you need to.
profile = "research"

# Adjust according to your region and AZ preference
availability_zone = "eu-west-2a"

# This is the smallest (read cheapest) instance type available. Works ok with
# this (micro are part of the 1 year free AWS offer).  
instance_type = "t2.nano"

# Make sure the name of your keypair matches the filename minus the .pub suffix.
key_name = "castopod-kp"

# You can call this what you like, it's only used to set the hostname
# on the Linux box
server_name = "castopod"

# Don't use this one. .
zone = "YOUR_DNS_ZONE"

# This is a pretty sensible default but again, change it if you like. The only 
# downside is it's long which may
# cause problems if you only have limited injection space.
castopod_zone = "testcastopod" # This will result in testcastopod.YOUR_DNS_ZONE

# Restrict this to places you will SSH from. The whole Internet is not all so friendly.
permitted_ssh_cidr_block = "0.0.0.0/0"
```

### Run Terraform

Now we're ready to run Terraform. First verify everything is ok by running plan:

`terraform plan`

Assuming you don't get any horrible errors you're ready to go.

![Image of yes kid](http://s2.quickmeme.com/img/ca/caeca14caf425c6de80bd94f29f63f0a1c5197fecabd50b1b1d916a79d9b8685.jpg)

`terraform apply`

Now sit back and behold the awesomeness of infrastructure as code.

The following operations will be performed:

* Create AWS security group permitting all Castopod traffic plus SSH to your 
  _permitted_ssh_cidr_block_ CIDR from the sepcified IP, or subnet.
* Create EC2 instance using Debian Bullseye image for your chosen region in your 
  default VPC.
* Create an A record for your chosen hostname in your AWS hosted zone pointing 
  to the IP address of new EC2 instance (if using th AWS template).
* Create an NS record for your chosen hostname pointing to the A record just 
  created (if using th AWS template).
* Run the ansible_castopod role for Ansible playbook on the EC2 instance to 
  install and configure Castopod.


### Using a "proper" TLS certificate

#### Using a CSR

If you would like to purchase a proper wildcard TLS certificate for use with
this server you need to generate a more appropriate CSR (the default values are
fairly generic). There is an Ansible playbook included in this folder to help
you.

Once you have the CSR you can go and purchase a Wildcard TLS certificate with
it and then upload it to your Burp server.

Here are the steps.

1. Edit [owntls.yml](owntls.yml) and set the different variables according to 
   what you want in your certificate
2. Delete the generated CSR: `rm castopod.csr`
3. `ansible-playbook -i inventory owntls.yml --tags tls`
4. Use the contents of the newly generated _castopod.csr_ file to purchase your 
   certificate.
5. Copy your new certificate to _castopod.crt_
6. Copy any intermediate CA cert bundle to _intermediate.crt_
7. `ansible-playbook -i inventory playbook.yml --tags setup,restart`

#### Using Lets'Encrypt

1. Use the certbot docker, in this case with an OVH hosted domain:

Create the .ovh.ini with the following informations:

```# OVH API credentials used by Certbot
dns_ovh_endpoint = ovh-eu
dns_ovh_application_key = MDAwMDAwMDAwMDAw
dns_ovh_application_secret = MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAw
dns_ovh_consumer_key = MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAw
```

Then use the Certbot docker like this:
```
docker run -it --rm --name certbot -v "$PWD/etc:/etc/letsencrypt" -v "$PWD/var/lib:/var/lib/letsencrypt" -v $PWD/.ovh.ini:/root/.ovh.ini certbot/dns-ovh certonly --dns-ovh --dns-ovh-credentials /root/.ovh.ini --dns-ovh-propagation-seconds 60 -d $DOMAIN --non-interactive --agree-tos --email $EMAIL
```

2. Copy the retrieved files and rename them into the local directory of the terraform-castopod:
  - cert.pem --> castopod.crt
  - chain.pem --> intermediate.crt
  - privkey.pem --> castopod.pk8

3. You may want to relauch the playbook to take into the account the update:

```
ansible-playbook -i inventory.yml playbook.yml --tags setup,restart
```


## Destroying

When you've had your fun, if you want to kill the whole thing just run:

`terraform destroy`
